package com.javagda17.spring.securitydemo.controller;

import com.javagda17.spring.securitydemo.model.AppUser;
import com.javagda17.spring.securitydemo.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin/")
public class AdminUserController {

    @Autowired
    private AppUserService appUserService;

    // adres na który trzeba wejść:
    // /admin/users
    @GetMapping("/users")
    public String getUserList(Model model) {
        List<AppUser> users = appUserService.getAllUsers();

        model.addAttribute("user_list", users);

        model.addAttribute("user_roles", users.stream()
                .map(user -> user.getRoles().stream().map(role -> role.getName()).collect(Collectors.toList()))
                // w wyniku powyższej instrukcji otrzymaliśmy stream list (ze stringami) uprawnień użytkowników
                .map(String::valueOf)
                // zamieniam każdą listę na tekst
                .collect(Collectors.toList()));

        // zwracamy admin (bo plik html znajduje się w katalogu admin)
        // a po nim /userlist (bo plik html nazywa się userlist)
        return "admin/userlist";
    }


}
